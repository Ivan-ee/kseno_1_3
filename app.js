const express = require('express');
const redis = require('redis');
const { generateRandomArray, sortArray, binarySearch } = require('./index.js');

const app = express();
const PORT = 8080;

const client = redis.createClient({
        url: "redis://redis:6379",
    }
);

client.on('error', err => {
    console.error('Redis Client Error', err);
});

async function getArrayFromRedis() {
    try {
        const array = await client.get('array');
        return array ? JSON.parse(array) : null;
    } catch (error) {
        console.error('Ошибка получения массива из Redis:', error);
        return null;
    }
}

app.listen(PORT, async () => {

    await client.connect();

    try {
        let array = await getArrayFromRedis();

        if (!array) {
            array = generateRandomArray(20);
            array = sortArray(array);
            await client.set('array', JSON.stringify(array));
            console.log('Массив успешно сохранен в Redis!');
        }

        console.log(`Сервер запущен на порту ${PORT}`);
    } catch (error) {
        console.error('Ошибка сохранения массива в Redis:', error);
        process.exit(1);
    }
});

app.get('/search/:number', async (req, res) => {
    try {
        const number = req.params.number;

        console.log(number)

        const array = await getArrayFromRedis();

        if (!array) {
            return res.status(500).json({ error: 'Массив не найден' });
        }

        const index = binarySearch(array, Number(number));
        res.json({ index, array });
    } catch (error) {
        console.error('Ошибка обработки запроса:', error);
        res.status(500).json({ error: 'Внутренняя ошибка сервера' });
    }
});