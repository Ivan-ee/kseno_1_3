// Update for ci test
function generateRandomArray(size) {
    const array = [];
    for (let i = 0; i < size; i++) {
        array.push(Math.floor(Math.random() * 100));
    }
    return array;
}

function sortArray(array) {
    return array.sort((a, b) => a - b);
}

function binarySearch(array, target) {
    let left = 0;
    let right = array.length - 1;

    while (left <= right) {
        let mid = Math.floor((left + right) / 2);
        let guess = array[mid];

        if (guess === target) {
            return mid;
        } else if (guess < target) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }

    return -1;
}

module.exports = { generateRandomArray, sortArray, binarySearch };

// const arrSize = 100;
// const target = process.argv[2];
//
// const array = generateRandomArray(arrSize);
// const sortedArray = sortArray(array);
//
// const index = binarySearch(sortedArray, parseInt(target));
//
// if (index !== -1) {
//     console.log(`Элемент ${target} найден в массиве c индексом ${index}.`);
// } else {
//     console.log(`Элемент ${target} не найден в массиве.`);
// }
// console.log(sortedArray)
