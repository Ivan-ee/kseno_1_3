const { generateRandomArray, sortArray, binarySearch } = require('./index.js');

describe('generateRandomArray', () => {
    it('should generate an array of specified size', () => {
        const size = 10;
        const array = generateRandomArray(size);
        expect(array).toHaveLength(size);
    });
});

describe('sortArray', () => {
    it('should sort the array in ascending order', () => {
        const array = [5, 3, 8, 1, 2];
        const sortedArray = sortArray(array);
        expect(sortedArray).toEqual([1, 2, 3, 5, 8]);
    });
});

describe('binarySearch', () => {
    it('should return the index of the target element if found in the array', () => {
        const array = [1, 3, 5, 7, 9];
        const target = 5;
        const index = binarySearch(array, target);
        expect(index).toBe(2);
    });

    it('should return -1 if the target element is not found in the array', () => {
        const array = [1, 3, 5, 7, 9];
        const target = 4;
        const index = binarySearch(array, target);
        expect(index).toBe(-1);
    });
});
